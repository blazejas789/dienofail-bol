if myHero.charName ~= "Gragas" then return end
require "VPrediction"

local VP = nil 
function OnLoad()
	VP = VPrediction()
	ts = TargetSelector(TARGET_LESS_CAST, 1200, DAMAGE_MAGIC)
	NidaleeConfig = scriptConfig("Simple Gragas", "Gragas")
	NidaleeConfig:addParam("combo", "Cast Q", SCRIPT_PARAM_ONKEYDOWN, false, 32)
	NidaleeConfig:addParam("accuracy", "Accuracy Slider", SCRIPT_PARAM_SLICE, 1, 0, 5, 0)
	ts.name = "Simple Gragas"
	NidaleeConfig:addTS(ts)
end

function OnTick()
	ts:update()
    if ts.target ~= nil and NidaleeConifg.combo and GetDistance(ts.target) <= 1100 then
        CastPosition,  HitChance,  Position = VP:GetLineCastPosition(target, 0.250, 200, 1100)
        if HitChance >= NidaleeConifg.accuracy and GetDistance(CastPosition) =< 950 then
            CastSpell(_Q, CastPosition.x, CastPosition.z)
        end
    end
end
