require "Prodiction"
require "FastCollision"


if myHero.charName ~= "Fizz" then return end
local QREADY = false
local WREADY = false
local EREADY = false
local RREADY = false
local ts = {}
local Prodict = ProdictManager.GetInstance()
local ProdictE = Prodict.AddProdictionObject()
local ProdictR = Prodict.AddProdictionObject(_R, 1100)
SkillE = AutoCarry.Skills:NewSkill(false, _E, 940, "Charm", AutoCarry.SPELL_LINEAR_COL, 0, false, false, 1.55, 240, 50, true)

local function getHitBoxRadius(target)
	return GetDistance(target, target.minBBox)
	--return GetDistance(target.minBBox, target.maxBBox)/4
end

local function CastR(unit, pos, spell)
	if GetDistance(pos) < 1100 and myHero:GetSpellData(_R).name == "LeonaSolarFlare" then
		CastSpell(_R, pos.x, pos.z)
	end
end