if myHero.charName ~= "Thresh" then return end
require "VPrediction"
require "Collision"


local VP = nil 
local ts = {}
local ThreshConfig = {}
function OnLoad()
VP = VPrediction()
ts = TargetSelector(TARGET_LESS_CAST, 1100, DAMAGE_MAGIC)
ThreshConfig = scriptConfig("Simple Thresh", "Thresh")
ThreshConfig:addParam("combo", "Q", SCRIPT_PARAM_ONKEYDOWN, false, 32)
ThreshConfig:addParam("accuracy", "Accuracy Slider", SCRIPT_PARAM_SLICE, 1, 0, 5, 0)
ProdictQCol = Collision(1075, 1200, 0.500, 80)
ts.name = "Simple Thresh"
ThreshConfig:addTS(ts)
end


function OnTick()
ts:update()
    if ts.target ~= nil and GetDistance(ts.target) <= 1075 and ThreshConfig.combo == true then
        CastPosition,  HitChance,  Position = VP:GetLineCastPosition(ts.target, 0.500, 80, 1800)
        local willCollide = ProdictQCol:GetMinionCollision(CastPosition, myHero)
        if ThreshConfig.accuracy == HitChance and not willCollide then
            CastSpell(_Q, CastPosition.x, CastPosition.z)
        end
    end
end