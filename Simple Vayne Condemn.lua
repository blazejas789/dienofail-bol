local enemyTable = GetEnemyHeroes()
function OnLoad()
	if myHero.charName ~= "Vayne" then return end
	require "Prodiction"
	Prod = ProdictManager.GetInstance()
	ProdE = Prod:AddProdictionObject(_E, 1000, 2200, 0.250, 0)
	ePos = nil
	--Menu
	PMenu = scriptConfig("Simple Vayne Condemn", "Vayne_Prodiction")
	PMenu:addParam("autoCondemn","Auto-Condemn Toggle:", SCRIPT_PARAM_ONKEYDOWN, false, 32)
	PMenu:addParam("pushDistance", "Push Distance", SCRIPT_PARAM_SLICE, 350, 0, 450, 0)
	PMenu:addParam("accuracy", "Accuracy", SCRIPT_PARAM_SLICE, 5, 1, 50, 15)
	PrintChat(" Simple Vayne Condemn by Dienofail loaded! ")
end


function OnTick()
	if myHero.dead then return end

	if PMenu.autoCondemn and myHero:CanUseSpell(_E) == READY then
		for i, enemyHero in ipairs(enemyTable) do
			if enemyHero ~= nil and enemyHero.valid and not enemyHero.dead and enemyHero.visible and GetDistance(enemyHero) <= 800 and GetDistance(enemyHero) > 0 then
				ePos = ProdE:GetPrediction(enemyHero)
			else
				ePos = nil
			end

			if ePos ~= nil then
				local checks = math.ceil(PMenu.accuracy)
				local checkDistance = math.ceil(PMenu.pushDistance/checks)
				local InsideTheWall = false
				for k=1, checks, 1 do
					local PushPosition = ePos + (Vector(ePos) - myHero):normalized()*(checkDistance*k)
					if IsWall(D3DXVECTOR3(PushPosition.x, PushPosition.y, PushPosition.z)) then
						InsideTheWall = true
						break
					end
				end
				if InsideTheWall then
					CastSpell(_E, enemyHero)
				end
			end
		end
	end
end
