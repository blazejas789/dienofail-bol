if myHero.charName ~= "Nami" then return end


if VIP_USER then 
	require 'Prodiction'
	Prodict = ProdictManager.GetInstance()
	ProdictQ = Prodict:AddProdictionObject(_Q, 875, math.huge, 0.8, 80)
else
	local ProdictQreg = TargetPrediction(875, math.huge, 800, 80, 0)
end


local NamiConfig = {}
local Wrange = 725
local Wbouncerange = 675
local Erange = 800
local ts = {}

function CastQ(unit, pos)
	if GetDistance(pos) <= 875 and myHero:CanUseSpell(_Q) == READY then
		CastSpell(_Q, pos.x, pos.z)
	end
end

function OnLoad()
	ts = TargetSelector(TARGET_PRIORITY, 1450, DAMAGE_MAGIC)
	NamiConfig = scriptConfig("Simple Nami by Dienofail", "Nami")
	NamiConfig:addParam("combo", "Combo", SCRIPT_PARAM_ONKEYDOWN, false, 32)
	NamiConfig:addParam("minbounces", "Minimum W Bounces", SCRIPT_PARAM_SLICE, 1, 1, 3, 0)
	NamiConfig:addParam("useE", "Use E", SCRIPT_PARAM_ONOFF, false)
	NamiConfig:addParam("useW", "Use W", SCRIPT_PARAM_ONOFF, false)
	for I = 1, heroManager.iCount do
		local hero = heroManager:GetHero(I)
		--activating donor callbacks
		if hero.team ~= myHero.team then
			if VIP_USER then
				ProdictQ:GetPredictionAfterImmobile(hero, CastQ)
			end
		end
	end
	ts.name = "Simple Nami"
	NamiConfig:addTS(ts)
	PrintChat("Simple Nami Loaded")
end

function OnTick()
	ts:update()
	if ValidTarget(ts.target) then
		if VIP_USER then
			if NamiConfig.combo and myHero:CanUseSpell(_Q) == READY then
				ProdictQ:GetPredictionCallBack(ts.target, CastQ)
			end
		else 
			if NamiConfig.combo and myHero:CanUseSpell(_Q) == READY then
				TargetPrediction__OnTick()
				local Qpos, minCol, nextHealth = ProdictQreg:GetPrediction(ts.target)
				CastSpell(_Q, Qpos.x, Qpos.z)
			end
		end
	end
	if NamiConfig.minbounces == 1 and GetDistance(ts.target) <= Wrange and NamiConfig.useW then
		CastSpell(_W, ts.target)
	end

	--Convoluted W logic happening. Stand back and watch it fail!
	if ValidTarget(ts.target) and NamiConfig.minbounces > 1 and NamiConfig.useW and NamiConfig.combo then 
		local currentbounces = 1 
		local bounce1hero
		local bounce2hero
		local bounceteam
		for I = 1, heroManager.iCount do
			local hero = heroManager:GetHero(I)
			if GetDistance(hero) <= Wrange and hero ~= myHero and not hero.dead then
				bounceoneteam = hero.team
				if bounceoneteam == myHero.team then
					if hero.maxHealth - hero.health >= 65 then
						--bouncepos1[x] = hero.x
						--bouncepos1[y] = hero.y
						--bouncepos1[z] = hero.z
						bounce1hero = hero
					end
				else
					bounce1hero = hero
				end
			end
		end
		if bounce1hero ~= nil then
			for I = 1, heroManager.iCount do
				local hero = heroManager:GetHero(I)
				currentdistance = GetDistance(hero, bounce1hero)
				if GetDistance(hero, bounce1hero) <= Wbouncerange and hero.team ~= bounce1hero.team and not hero.dead and hero.charName ~= bounce1hero.charName then
					bounce2hero = hero
					if NamiConfig.minbounces == 2 then
						CastSpell(_W, bounce1hero)
					end
				end
			end
		end

		if bounce1hero ~= nil and bounce2hero ~= nil then
			for I = 1, heroManager.iCount do
				local hero = heroManager:GetHero(I)
				if GetDistance(hero, bounce2hero) <= Wbouncerange and hero.team == bounce1hero.team and not hero.dead and hero.charName ~= bounce1hero.charName and hero.charName ~= bounce2hero.charName then
					if NamiConfig.minbounces == 3 then
						CastSpell(_W, bounce1hero)
					end
				end
			end
		end
	end
	
	if ValidTarget(ts.target) and NamiConfig.useE and NamiConfig.combo then
		allytable = GetAllyHeroes()
		local currenthero = myHero
		for i, v in ipairs(allytable) do
			if v.damage > myHero.damage and GetDistance(v) <= Erange and not v.dead then
				currenthero = v
			end
		end 
		CastSpell(_E, currenthero)
	end 
end

function OnDraw()
	if ts.target ~= nil then
		DrawCircle(myHero.x, myHero.y, myHero.z, 725, 0x7F006E)
		DrawCircle(myHero.x, myHero.y, myHero.z, 875, 0xCC0000)
	end
end