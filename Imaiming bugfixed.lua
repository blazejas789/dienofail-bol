--[[
    I'M Aiming 0.2.1 by Klokje
    ========================================================================
    
    Change log:
    0.1:
        - Initial release

    0.2 
       - trying to fix the fps, people test please
    0.2.1 
        - Edit the targetselecter and keys
    0.3 


]]
-- Globals ---------------------------------------------------------------------
require "Collision"
require 'Prodiction'

_G.Champs = {
    ["Nidalee"] = {
        [_Q] = { speed = 1300, delay = 0.125, range = 1500, minionCollisionWidth = 80},
    },
    ["Karthus"] = {
        [_Q] = { speed = 1750, delay = 0.25, range = 875, minionCollisionWidth = 0},
    },
    ["Zyra"] = {
         [_E] = { speed = 1150, delay = 0.250, range = 1150, minionCollisionWidth = 0}
    },
    ["Blitzcrank"] = {
        [_Q] = { speed = 1800, delay = 0.250, range = 1050, minionCollisionWidth =  90}
    },
    ["Cassiopeia"] = {
        [_Q] = { speed = math.huge, delay = 0.535, range = 850, minionCollisionWidth = 0}
    },
    ["Gragas"] = {
        [_Q] = { speed = 1000, delay = 0.250, range = 1100, minionCollisionWidth = 0}
    },
    ["Rumble"] = {
        [_E] = { speed = 2000, delay = 0.250, range = 950, minionCollisionWidth = 80}
    },
    ["Kennen"] = {
        [_Q] = { speed = 1700, delay = 0.250, range = 1050, minionCollisionWidth = 70}
    },
    ["Morgana"] = {
        [_Q] = { speed = 1200, delay = 0.250, range = 1300, minionCollisionWidth = 80}
    },
    ["DrMundo"] = {
        [_Q] = { speed = 2000, delay = 0.250, range = 1050, minionCollisionWidth = 80}
    },
    ["Lux"] = {
        [_Q] = { speed = 1200, delay = 0.245, range = 1300, minionCollisionWidth = 50},
        [_E] = { speed = 1400, delay = 0.245, range = 1100, minionCollisionWidth = 0},
        [_R] = { speed = math.huge, delay = 0.245, range = 3500, minionCollisionWidth = 0}
    },
    ["Ezreal"] = {
        [_Q] = { speed = 2000, delay = 0.251, range = 1200, minionCollisionWidth = 80},
        [_W] = { speed = 1600, delay = 0.25, range = 1050, minionCollisionWidth = 0},
        [_R] = { speed = 2000, delay = 1, range = 20000, minionCollisionWidth = 0}
    },
    ["Leona"] = {
        [_E] = { speed = 2000, delay = 0.250, range = 900, minionCollisionWidth = 0},
        [_R] = { speed = 2000, delay = 0.250, range = 1200, minionCollisionWidth = 0},
    },
    ["LeeSin"] = {
        [_Q] = { speed = 1800, delay = 0.250, range = 1100, minionCollisionWidth = 100}
    },
    ["Karma"] = {
        [_Q] = { speed = 1700, delay = 0.250, range = 1050, minionCollisionWidth = 80}
    },
    ["Syndra"] = {
        [_Q] = { speed = math.huge, delay = 0.400, range = 800, minionCollisionWidth = 0}
    },
    ["Elise"] = {
        [_E] = { speed = 1450, delay = 0.250, range = 975, minionCollisionWidth = 80}
    },
    ["Amumu"] = {
        [_Q] = { speed = 2000, delay = 0.250, range = 1100, minionCollisionWidth = 100}
    },
    ["TwistedFate"] = {
        [_Q] = { speed = 1450, delay = 0.200, range = 1450, minionCollisionWidth = 0}
    },
    ["KogMaw"] = {
        [_R] = { speed = math.huge, delay = 0.700, range = GetSpellData(_R).range, minionCollisionWidth = 0}
    },
    ["Thresh"] = {
        [_Q] = { speed = 1900, delay = 0.500, range = 1100, minionCollisionWidth = 100}
    },
    ["Ahri"] = {
        [_Q] = { speed = 2500, delay = 0.25, range = 900, minionCollisionWidth = 0},
        [_E] = { speed = 1500, delay = 0.25, range = 1000, minionCollisionWidth = 80}
    },
    ["Olaf"] = {
        [_Q] = { speed = 1600, delay = 0.25, range = 1000, minionCollisionWidth = 0}
    },
    ["Chogath"] = {
        [_Q] = { speed = 950, delay = 0, range = 950, minionCollisionWidth = 0}
    },
    ["Nautilus"] = {
        [_Q] = { speed = 2000, delay = 0.250, range = 1080, minionCollisionWidth = 100}
    },
    ["Urgot"] = {
        [_Q] = { speed = 1600, delay = 0.175, range = 1000, minionCollisionWidth = 100},
        [_E] = { speed = 1750, delay = 0.25, range = 900, minionCollisionWidth = 0}
    },
    ["Caitlyn"] = {
        [_Q] = { speed = 2200, delay = 0.625, range = 1300, minionCollisionWidth = 0}
    },
    ["Brand"] = {
        [_Q] = { speed = 1600, delay = 0.625, range = 1100, minionCollisionWidth = 90},
        [_W] = { speed = 900, delay = 0.25, range = 1100, minionCollisionWidth = 0},
    }
}



_G.predictions = {}
_G.str = { [_Q] = "Q", [_W] = "W", [_E] = "E", [_R] = "R" }
_G.keybindings = { [_Q] = "Z", [_W] = "X", [_E] = "C", [_R] = "V" }
_G.Config = scriptConfig("I'M Aiming: Settings", "ImAiming")
_G.ConfigType = SCRIPT_PARAM_ONKEYDOWN

_G.ts = TargetSelector(TARGET_LOW_HP, 1500, DAMAGE_MAGIC, true)


 -- Code ------------------------------------------------------------------------
function OnLoad()
    PrintChat(" >> I'M Aiming by Klokje")

    wp = ProdictManager.GetInstance() 
    if Champs[myHero.charName] ~= nil then 
        for i, spell in pairs(Champs[myHero.charName]) do
            Config:addParam(str[i], "Predict " .. str[i], ConfigType, false, GetKey(keybindings[i]))
            predictions[str[i]] = wp:AddProdictionObject(i, spell.range, spell.speed, spell.delay, spell.minionCollisionWidth, myHero, 
                function(unit, pos, castspell) 
                    if GetDistanceSqr(unit) < castspell.RangeSqr and myHero:CanUseSpell(castspell.Name) == READY then 
                        if spell.minionCollisionWidth ~= 0 then
                            local collition = Collision(spell.range, spell.speed, spell.delay, spell.minionCollisionWidth)
                            if not collition:GetMinionCollision(pos, myHero) then 
                                CastSpell(castspell.Name, pos.x, pos.z)
                            end 
                        else 
                            CastSpell(castspell.Name, pos.x, pos.z)
                        end 
                    end 
                end)
        end 
    end 
    ts.name = "ImAiming"
    Config:addTS(ts)
end

function OnTick()
    ts:update()
    if ts.target ~= nil then 
        for i, param in pairs(predictions) do
            if Config[i] then 
                param:EnableTarget(ts.target, true)
            end 
        end 
    end 
end 