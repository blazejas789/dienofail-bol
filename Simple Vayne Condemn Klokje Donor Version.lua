if myHero.charName ~= "Vayne" then return end
local enemyTable = GetEnemyHeroes()
function OnLoad()
	require "Prodiction"
	Prod = ProdictManager.GetInstance()
	ProdE = Prod:AddProdictionObject(_E, 1000, 2200, 0.250, 0)
	ePos = nil
	for i = 1, heroManager.iCount do
		local enemyHero = heroManager:GetHero(i)
		if enemyHero.team ~= myHero.team then
			ProdE:GetPredictionAfterImmobile(enemyHero, StunAfterStun)
			ProdE:GetPredictionAfterDash(enemyHero, StunAfterStun)
			ProdE:GetPredictionOnDash(enemyHero, StunAfterStun)
		end
	end
	--Menu
	PMenu = scriptConfig("Simple Vayne Condemn Donor Version", "Vayne_Prodiction")
	PMenu:addParam("autoCondemn","Auto-Condemn Toggle:", SCRIPT_PARAM_ONKEYDOWN, false, 32)
	PMenu:addParam("pushDistance", "Push Distance", SCRIPT_PARAM_SLICE, 300, 0, 450, 0)
	PMenu:addParam("accuracy", "Accuracy", SCRIPT_PARAM_SLICE, 5, 1, 50, 0)
	PMenu:addParam("After_Dash_Call_Back", "Dash global callbacks", SCRIPT_PARAM_ONOFF, false)
	PMenu:addParam("After_Dash_Call_Back_Min_Distance", "Dash global callbacks slider", SCRIPT_PARAM_SLICE, 50, 0, 450, 0)
	PrintChat(" Simple Vayne Condemn Prodiction Donor Version by Dienofail loaded! ")
end


function OnTick()
	if myHero.dead then return end

	if Pmenu.After_Dash_Call_Back then 
		for i, enemyHero in ipairs(enemyTable) do
			ProdE:GetPredictionOnDash(enemyHero, PressE)
			ProdE:GetPredictionAfterDash(enemyHero, PressE)
		end
	else
		for i, enemyHero in ipairs(enemyTable) do
			ProdE:GetPredictionOnDash(enemygHero, PressE, false)
			ProdE:GetPredictionAfterDash(enemyHero, PressE, false)
		end
	end

	if PMenu.autoCondemn and myHero:CanUseSpell(_E) == READY then
		for i, enemyHero in ipairs(enemyTable) do
			if enemyHero ~= nil and enemyHero.valid and not enemyHero.dead and enemyHero.visible and GetDistance(enemyHero) <= 715 and GetDistance(enemyHero) > 0 then 
				ProdE:GetPredictionCallBack(enemyHero, StunAfterStun)
			end
		end
	end
end

function StunAfterStun(target, pos)
	if pos ~= nil and myHero:CanUseSpell(_E) == READY and GetDistance(target) < 715 then
		local checks = math.ceil(PMenu.accuracy)
		local checkDistance = math.ceil(PMenu.pushDistance/checks)
		local InsideTheWall = false
		for k=1, checks, 1 do
			local PushPosition = pos + (Vector(pos) - myHero):normalized()*(checkDistance*k)
			if IsWall(D3DXVECTOR3(PushPosition.x, PushPosition.y, PushPosition.z)) then
				InsideTheWall = true
				break
			end
		end
		if InsideTheWall then
			CastSpell(_E, target)
		end
	end
end


function PressE(target, pos)
	if pos ~= nil and myHero:CanUseSpell(_E) == READY and GetDistance(target) < Pmenu.After_Dash_Call_Back_Min_Distance then
		CastSpell(_E, target)
	end
end
