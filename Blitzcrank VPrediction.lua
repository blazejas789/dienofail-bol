if myHero.charName ~= "Blitzcrank" then return end
require "VPrediction"
require "Collision"


local VP = nil 
local ts = {}
local BlitzcrankConfig = {}
function OnLoad()
VP = VPrediction()
ts = TargetSelector(TARGET_LESS_CAST, 1100, DAMAGE_MAGIC)
BlitzcrankConfig = scriptConfig("Simple Blitzcrank", "Blitzcrank")
BlitzcrankConfig:addParam("combo", "Q", SCRIPT_PARAM_ONKEYDOWN, false, 32)
BlitzcrankConfig:addParam("accuracy", "Accuracy Slider", SCRIPT_PARAM_SLICE, 1, 0, 5, 0)
ProdictQCol = Collision(1050, 1800, 0.250, 90)
ts.name = "Simple Blitzcrank"
BlitzcrankConfig:addTS(ts)
end


function OnTick()
ts:update()
    if ts.target ~= nil and GetDistance(ts.target) <= 1050 and BlitzcrankConfig.combo == true then
        CastPosition,  HitChance,  Position = VP:GetLineCastPosition(ts.target, 0.250, 90, 1800)
        local willCollide = ProdictQCol:GetMinionCollision(CastPosition, myHero)
        if BlitzcrankConfig.accuracy == HitChance and not willCollide then
            CastSpell(_Q, CastPosition.x, CastPosition.z)
        end
    end
end