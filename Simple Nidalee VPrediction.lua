if myHero.charName ~= "Nidalee" then return end
require "VPrediction"
require "Collision"

local VP = nil 
local ts = {}
local NidaleeConfig = {}
function OnLoad()
	VP = VPrediction()
	ts = TargetSelector(TARGET_LESS_CAST, 1700, DAMAGE_MAGIC)
	NidaleeConfig = scriptConfig("Simple Nidalee", "Nidalee")
	NidaleeConfig:addParam("combo", "Q", SCRIPT_PARAM_ONKEYDOWN, false, 32)
	NidaleeConfig:addParam("accuracy", "Accuracy Slider", SCRIPT_PARAM_SLICE, 1, 0, 5, 0)
	ProdictQCol = Collision(1700, 1300, 0.125, 60)
	ts.name = "Simple Nidalee"
	NidaleeConfig:addTS(ts)
end

function OnTick()
	ts:update()
    if ts.target ~= nil and GetDistance(ts.target) <= 1700 and NidaleeConfig.combo == true then
        CastPosition,  HitChance,  Position = VP:GetLineCastPosition(ts.target, 0.125, 60, 1300)
        local willCollide = ProdictQCol:GetMinionCollision(CastPosition, myHero)
        if NidaleeConfig.accuracy == HitChance and not willCollide then
            CastSpell(_Q, CastPosition.x, CastPosition.z)
        end
    end
end
