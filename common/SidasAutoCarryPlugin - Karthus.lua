if myHero.charName ~= "Karthus" then return end
require 'Prodiction'
local Prodict = {}

function PluginOnLoad()
	--Menu
	mainLoad()
	mainMenu()
end
function PluginOnTick()
	Checks()
	local enemyTable = {}
	for I = 1, heroManager.iCount do
		local hero = heroManager:GetHero(I)
		if hero.team ~= myHero.team and not hero.dead then
			table.insert(enemyTable, hero)
		end
	end
	current_E_counter = 0
	current_R_counter = 0
	current_R_distance = math.huge
	if Target and AutoCarry.MainMenu.AutoCarry then
		--PrintChat("Debug 1")
		if GetDistance(Target) < 850 and QREADY then
				--PrintChat("Can Cast Q")
				ProdictQ:GetPredictionCallBack(Target, CastQ)

		end
		--PrintChat("Debug 2")
		if GetDistance(Target) < 1050 and WREADY then
				--PrintChat("Can Cast W")
				ProdictW:GetPredictionCallBack(Target, CastW)
		end
		--PrintChat("Debug 3")
		if EREADY and Menu.useE then 
			for i, enemyHero in ipairs(enemyTable) do
				if GetDistance(enemyHero, myHero) < 550 then
					current_E_counter = current_E_counter + 1
				end
			end
		end
		--PrintChat("Current E counter is " .. tostring(current_E_counter))

		if current_E_counter >= Menu.minE and not isPressedE then
			--PrintChat("Can Cast E " .. tostring(isPressedE))
			CastSpell(_E)
		--[[elseif current_E_counter < Menu.minE and isPressedE then
			CastSpell(_E)
			--PrintChat("Can 2nd Cast E " .. tostring(isPressedE))
		end]]
	end


	if current_E_counter < Menu.minE and isPressedE then
		CastSpell(_E)
		--PrintChat("Can 2nd Cast E " .. tostring(isPressedE))
	end

	if RREADY and Menu.useR and Menu.useRks then
		for i, enemyHero in ipairs(enemyTable) do
			if getDmg("R", enemyHero, myHero) > enemyHero.health then
				current_R_counter = current_R_counter + 1
				--PrintChat("R counter incremented " .. tostring(current_R_counter))
			end
			--[[if GetDistance(enemyHero, myHero) < current_R_distance then
				current_R_distance = GetDistance(enemyHero, myHero)
				--PrintChat("R distance incremented " .. tostring(current_R_distance))
			end]]
		end
	end
	--PrintChat("Debug 4")
	if current_R_counter >= Menu.minR and Menu.useRks and Menu.useR and current_R_distance > Menu.minRdistance and RREADY then
		--PrintChat("Can Cast R")
		CastSpell(_R)
	end

	if Menu.Ralert and RREADY and current_R_counter > 0 then
		if current_R_counter == 1 then
			local alert = PrintAlert("1 ENEMY KILLALBE", 1, 255, 255, 255)
		elseif current_R_counter == 2 then
			local alert = PrintAlert("2 ENEMIES KILLALBE", 1, 255, 255, 255)
		elseif current_R_counter == 3 then
			local alert = PrintAlert("3 ENEMIES KILLALBE", 1, 255, 255, 255)
		elseif current_R_counter == 4 then
			local alert = PrintAlert("4 ENEMIES KILLALBE", 1, 255, 255, 255)
		elseif current_R_counter == 5 then
			local alert = PrintAlert("5 ENEMIES KILLALBE", 1, 255, 255, 255)
		end
	end

end
end

local function getHitBoxRadius(target)
	return GetDistance(target, target.minBBox)
end

function CastQ(unit, pos)
	if GetDistance(pos) - getHitBoxRadius(unit)/2 < qmaxRange then
			CastSpell(_Q, pos.x, pos.z)
	end
end

function CastW(unit, pos)
	if GetDistance(pos) - getHitBoxRadius(unit)/2 < wRange then
			CastSpell(_W, pos.x, pos.z)
	end
end


function mainLoad()
	if AutoCarry.Skills then IsSACReborn = true else IsSACReborn = false end
	if IsSACReborn then AutoCarry.Skills:DisableAll() end
	Carry = AutoCarry.MainMenu
	Menu = AutoCarry.PluginMenu
	min_hit_chance = 1
	qtotalcasttime = 400 --not sure about this value yet
	qmaxRange = 875
	qminRange = 250
	qWidth = 55
	qSpeed = 1500
	eRange = 550
	wWidth = 150
	wRange = 1000
	rRange = 800
	QREADY = false
	WREADY = false 
	EREADY = false
	RREADY = false
	isPressedE = false 
	Prodict = ProdictManager.GetInstance()
	ProdictQ = Prodict:AddProdictionObject(_Q, 875, 1750, 0.61, 50)
	ProdictW = Prodict:AddProdictionObject(_W, 1000, math.huge, 0.656, 0)
	ignite = ((myHero:GetSpellData(SUMMONER_1).name:find("SummonerDot") and SUMMONER_1) or (myHero:GetSpellData(SUMMONER_2).name:find("SummonerDot") and SUMMONER_2) or nil)
	PrintChat("Sidas Autocarry Karthus Plugin v0.01 by Dienofail loaded")
	AutoCarry.Crosshair:SetSkillCrosshairRange(1100) -- Set the spell target selector range
	AdvancedCallback:bind('OnGainBuff', function(unit, buff) OnGainBuff(unit, buff) end)
	AdvancedCallback:bind('OnLoseBuff', function(unit, buff) OnLoseBuff(unit, buff) end)
end

function mainMenu()
	Menu:addParam("useQ", "Use (Q)", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useW", "Use (W)", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useE", "Use (E)", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useR", "Use (R)", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useRks", "Use (R) ks", SCRIPT_PARAM_ONOFF, false)
	Menu:addParam("drawQ", "Draw (Q)", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("drawW", "Draw (W)", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("minE", "Minimum heroes on (E) cast", SCRIPT_PARAM_SLICE, 1, 0, 5, 0)
	Menu:addParam("minR", "Minimum heroes on (R) cast", SCRIPT_PARAM_SLICE, 1, 0, 5, 0)
	Menu:addParam("minRdistance", "Minimum distance on closest enemy on (R) cast", SCRIPT_PARAM_SLICE, 0, 0, 2000, 0)
	Menu:addParam("Ralert", "R alert on or off", SCRIPT_PARAM_ONOFF, true)
end


function Checks()
	QREADY = (myHero:CanUseSpell(_Q) == READY)
	WREADY = (myHero:CanUseSpell(_W) == READY)
	EREADY = (myHero:CanUseSpell(_E) == READY)
	RREADY = (myHero:CanUseSpell(_R) == READY)
	if myHero:GetSpellData(SUMMONER_1).name:find("SummonerDot") then ignite = SUMMONER_1
	elseif myHero:GetSpellData(SUMMONER_2).name:find("SummonerDot") then ignite = SUMMONER_2 end
	IGNITEReady = (ignite ~= nil and myHero:CanUseSpell(ignite) == READY)
	if IsSACReborn then Target = AutoCarry.Crosshair:GetTarget() else Target = AutoCarry.GetAttackTarget() end
end

function PluginOnDraw()
	if Menu.drawQ then
		DrawCircle(myHero.x, myHero.y, myHero.z, qmaxRange, 0x7F006E)
	end
	if Menu.drawW then
		DrawCircle(myHero.x, myHero.y, myHero.z, wRange, 0x7F006E)
	end
end


function OnGainBuff(unit, buff)
	if unit.isMe and buff.name == 'Defile' then
		----PrintChat("Gained")
		isPressedE= true
		Qstartcasttime = os.clock()
	end
end

function OnLoseBuff(unit, buff)
	if unit.isMe and buff.name == 'Defile' then
		----PrintChat("Lost")
		isPressedE = false
		Qcasttime = 0
	end
end