if myHero.charName ~= "Nami" then return end

function PluginOnLoad()
	--Menu
	mainLoad()
	mainMenu()
end


function PluginOnTick()
	Checks()
	if Target then
		if Menu.useQ and Menu.killsteal then
			CastQ()
		end
		if Menu.useR and Menu.killsteal then
			CastRignite()
			CastR()
			AutoIgnite()
		end

		if AutoCarry.MainMenu.AutoCarry then
			if Menu.useQ and GetDistance(Target) <= qRange and QREADY then
				SkillQ:Cast(Target)
			end

			if Menu.useQ and GetDistance(Target) <= Menu.qdistance and QREADY then
				CastSpell(_Q)
			end

			if Menu.useW and WREADY and GetDistance(Target) <= rRange then
				CastSpell(_W)
			end

			if Menu.useE and GetDistance(Target) <= eRange and EREADY and Qbeingcast == false then
				CastSpell(_E)
			end
		end

	end

end


function CastR()
	if not RREADY then return true end

	for _, enemy in pairs(AutoCarry.EnemyTable) do
		if ValidTarget(enemy, rRange) and getDmg("R", enemy, myHero) >= enemy.health then CastSpell(_R, enemy) end
	end
end

function CastQ()

	if not QREADY then return true end

	for _, enemy in pairs(AutoCarry.EnemyTable) do
		if ValidTarget(enemy, qRange) and getDmg("Q", enemy, myHero) >= enemy.health then CastSpell(_Q, enemy) end
	end
end


function CastRignite()
	if not RREADY then return true end
	if not IGNITEReady then return true end
	for _, enemy in pairs(AutoCarry.EnemyTable) do
		if ValidTarget(enemy, rRange) and getDmg("R", enemy, myHero) + getDmg("IGNITE", enemy, myHero) >= enemy.health then
			CastSpell(_R, enemy)
			CastSpell(ignite, enemy)
		end
	end
end

function AutoIgnite()
	if not IGNITEReady then return true end

	for _, enemy in pairs(AutoCarry.EnemyTable) do
		if ValidTarget(enemy, 600) then
			if getDmg("IGNITE",enemy,myHero) >= enemy.health then
				CastSpell(ignite, enemy)
			end
		end
	end
end

function mainLoad()
	if AutoCarry.Skills then IsSACReborn = true else IsSACReborn = false end
	if IsSACReborn then AutoCarry.Skills:DisableAll() end
	Carry = AutoCarry.MainMenu
	Menu = AutoCarry.PluginMenu
	qRange = 300
	eRange = 165
	rRange = 400
	SkillQ = AutoCarry.Skills:NewSkill(false, _Q, qRange, "Decisive Strike", AutoCarry.SPELL_TARGETED, 0, true, true, math.huge, 240, 0, 0)
	ignite = ((myHero:GetSpellData(SUMMONER_1).name:find("SummonerDot") and SUMMONER_1) or (myHero:GetSpellData(SUMMONER_2).name:find("SummonerDot") and SUMMONER_2) or nil)
	PrintChat("Sidas Autocarry Garen Plugin by Dienofail loaded")
	AdvancedCallback:bind('OnGainBuff', function(unit, buff) OnGainBuff(unit, buff) end)
    AdvancedCallback:bind('OnLoseBuff', function(unit, buff) OnLoseBuff(unit, buff) end)
end

function mainMenu()
	Menu:addParam("useQ", "Use (Q)", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useW", "Use (W)", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useE", "Use (E)", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useR", "Use (R)", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("useQE", "Use E after Q", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("qdistance", "Use (Q) Chase Distance", SCRIPT_PARAM_SLICE, 150, 0, 1000, 150)
	Menu:addParam("killsteal", "Kill Steal with R", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("drawQ", "Draw (Q)", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("drawR", "Draw (R)", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("Qslows", "Cast Q on slows", SCRIPT_PARAM_ONOFF, true)
end

function Checks()
	QREADY = (myHero:CanUseSpell(_Q) == READY)
	WREADY = (myHero:CanUseSpell(_W) == READY)
	EREADY = (myHero:CanUseSpell(_E) == READY)
	RREADY = (myHero:CanUseSpell(_R) == READY)
	if myHero:GetSpellData(SUMMONER_1).name:find("SummonerDot") then ignite = SUMMONER_1
	elseif myHero:GetSpellData(SUMMONER_2).name:find("SummonerDot") then ignite = SUMMONER_2 end
	IGNITEReady = (ignite ~= nil and myHero:CanUseSpell(ignite) == READY)
	if IsSACReborn then Target = AutoCarry.Crosshair:GetTarget() else Target = AutoCarry.GetAttackTarget() end

end

function PluginOnAnimation(object, animation)
	if object.isMe and animation == "Spell1" then
		if Menu.useQE and EREADY then CastSpell(_E) end
	end
end

function PluginOnDraw()
	DrawCircle(myHero.x, myHero.y, myHero.z, rRange, 0x7F006E)

end

function OnGainBuff(unit, buff)
    if buff.name == 'GarenQ' and unit.isMe then
    	Qbeingcast = true
    end

    if buff.type == 10 and myHero:CanUseSpell(_Q) == READY and unit.isMe then
    	CastSpell(_Q)
    end
end

function OnLoseBuff(unit, buff)
    if buff.name == 'GarenQ' and unit.isMe then
    	Qbeingcast = false
    end
end