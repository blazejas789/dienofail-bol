if myHero.charName ~= "Leona" then return end
local QREADY = false
local WREADY = false
local EREADY = false
local RREADY = false
local ProdictE = TargetPrediction(875, 1.0, 250, 0)
local ProdictR = TargetPrediction(1200, math.huge, 520, 0)
local triggerbarrel = false
local ts = {}
local LeonaConfig = {}
--SHARED--
local function getHitBoxRadius(target)
	return GetDistance(target, target.minBBox)
	--return GetDistance(target.minBBox, target.maxBBox)/4
end
--END SHARED--


local function CastE(unit, pos, spell)
	if GetDistance(pos) - getHitBoxRadius(unit)/2 < 875 and myHero:GetSpellData(_E).name == "LeonaZenithBlade" then
		CastSpell(_E, pos.x, pos.z)
	end
end

local function CastR(unit, pos, spell)
	if GetDistance(pos) < 1200 and myHero:GetSpellData(_R).name == "LeonaSolarFlare" then
		CastSpell(_R, pos.x, pos.z)
	end
end


function OnLoad()
    print(myHero:GetSpellData(_E).name)
	ts = TargetSelector(TARGET_LESS_CAST, 1200, DAMAGE_MAGIC)
	LeonaConfig = scriptConfig("Simple Leona", "LeonaAdv")
	local HK = string.byte("C")
	local HKC = string.byte("X")
	local HKV = string.byte("Z")
	LeonaConfig:addParam("E", "Cast EQ", SCRIPT_PARAM_ONKEYDOWN, false, HK)
	LeonaConfig:addParam("C", "Cast EWQR", SCRIPT_PARAM_ONKEYDOWN, false, 32)
	LeonaConfig:addParam("R", "Cast R", SCRIPT_PARAM_ONKEYDOWN, false, HKV)
	LeonaConfig:addTS(ts)
	
	ts.name = "Simple Leona"
	-- for I = 1, heroManager.iCount do
	-- 	local hero = heroManager:GetHero(I)
	-- 	if hero.team ~= myHero.team then
	-- 		ProdictE:GetPredictionAfterImmobile(hero,  CastE)
	-- 		ProdictE:GetPredictionOnDash(hero,  CastE)
	-- 		ProdictE:GetPredictionAfterDash(hero, CastE)
	-- 	end
	-- end
end

function OnTick()
	Prediction__OnTick()
	ts:update()
	if ts.target ~= nil and LeonaConfig.E and GetDistance(ts.nextPosition) - getHitBoxRadius(ts.target)/2 < 875 and myHero:CanUseSpell(_E) == READY then
		local Epos, minCol, nextHealth = ProdictE:GetPrediction(ts.target)
		CastSpell(_E, Epos.x, Epos.z)
		CastSpell(_Q)
	end

	
	if ts.target ~= nil and LeonaConfig.C and GetDistance(ts.nextPosition) - getHitBoxRadius(ts.target)/2 < 875 and myHero:CanUseSpell(_E) == READY then
		local Epos, minCol, nextHealth = ProdictE:GetPrediction(ts.target)
		CastSpell(_E, Epos.x, Epos.z)
		CastSpell(_W) 
    	CastSpell(_Q)
	end
	
	if ts.target ~= nil and LeonaConfig.R and myHero:CanUseSpell(_R) == READY and GetDistance(ts.target) - getHitBoxRadius(ts.target)/2 < 1300 then
		local Rpos, RminCol, RnextHealth = ProdictR:GetPrediction(ts.target)
		CastSpell(_R, Rpos.x, Rpos.z)
	end

	QREADY = (myHero:CanUseSpell(_Q) == READY)
	WREADY = (myHero:CanUseSpell(_Q) == READY)
	EREADY = (myHero:CanUseSpell(_Q) == READY)
	RREADY = (myHero:CanUseSpell(_Q) == READY)
	
	
end

function OnGainBuff(unit, buff)

	 if unit == ts.target and buff.type == 11 and buff.name == "leonazenithbladeroot" and LeonaConfig.E then
     CastSpell(_Q)
     end

	 if unit == ts.target and buff.type == 11 and buff.name == "leonazenithbladeroot" and LeonaConfig.C then
     CastSpell(_W) 
     CastSpell(_Q)
     end
        
     if unit == ts.target and buff.type == 5 and buff.name == "Stun" and LeonaConfig.C then
     CastSpell(_R, unit.x, unit.z)
     end
    
end



function OnDraw()
	if ts.target ~= nil then
		local dist = getHitBoxRadius(ts.target)/2
		DrawCircle(myHero.x, myHero.y, myHero.z, 875, 0x7F006E)
		DrawCircle(myHero.x, myHero.y, myHero.z, 1200, 0x7F006E)
		if GetDistance(ts.target) - dist < 875 then
			DrawCircle(ts.target.x, ts.target.y, ts.target.z, dist, 0x7F006E)
		end
	end
end